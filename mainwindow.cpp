#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QMessageBox"
#include "QKeyEvent"
#include "QString"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->ui->label->setFocus();

    ui->centralWidget->setFixedSize(321,521);
    MainWindow::setMaximumSize(321, 521);

    /**
     * Connecting all the digits buttons to digitPressed Function
     */
    connect(ui->button_0, SIGNAL(released()), this, SLOT(digitPressed()));
    connect(ui->button_1, SIGNAL(released()), this, SLOT(digitPressed()));
    connect(ui->button_2, SIGNAL(released()), this, SLOT(digitPressed()));
    connect(ui->button_3, SIGNAL(released()), this, SLOT(digitPressed()));
    connect(ui->button_4, SIGNAL(released()), this, SLOT(digitPressed()));
    connect(ui->button_5, SIGNAL(released()), this, SLOT(digitPressed()));
    connect(ui->button_6, SIGNAL(released()), this, SLOT(digitPressed()));
    connect(ui->button_7, SIGNAL(released()), this, SLOT(digitPressed()));
    connect(ui->button_8, SIGNAL(released()), this, SLOT(digitPressed()));
    connect(ui->button_9, SIGNAL(released()), this, SLOT(digitPressed()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

/**
 * Custom functions
 */
void MainWindow::numberPressed(QString number){
    QLineEdit *field;
    bool ok = true;

    if(ui->line1->isEnabled()){
        field = ui->line1;
    } else {
        field = ui->line2;
    }

    if((number.toInt()==0)&&(field->text().length()==0)){
        ok = false;
    }

    if(ok){
        field->setText(field->text().append(number));
    }
}

void MainWindow::digitPressed(){
    QPushButton *button = (QPushButton*) sender();
    this->numberPressed(button->text());
}

void MainWindow::execLastAction(){
    switch (operatie) {
    case PLUS:
        _numarStocat = _numarStocat + _numarCurent;
        this->displayStoredNumber();
        break;

    case MINUS:
        _numarStocat = _numarStocat - _numarCurent;
        this->displayStoredNumber();
        break;

    case MULTI:
        _numarStocat = _numarStocat * _numarCurent;
        this->displayStoredNumber();
        break;

    case DIVIDE:
        if(_numarCurent.getNumarator()==0){
            QMessageBox msgBox;
            msgBox.setText("Nu se poate imparti la 0!");
            msgBox.exec();
        } else {
            _numarStocat = _numarStocat / _numarCurent;
            this->displayStoredNumber();
        }
        break;

    default:
        _numarStocat = _numarCurent;
        _numarCurent.clear();
        break;
    }
}

void MainWindow::setCurrentNumber(){
    long long numitor;
    long long numarator = ui->line1->text().toLongLong();

    if(!ui->line2->text().toLongLong()){
        numitor = 1;
    } else {
        numitor = ui->line2->text().toLongLong();
    }

    _numarCurent.setNumber(numarator, numitor);
}

void MainWindow::displayCurrentNumber(){
    ui->line1->setText(QString::number(_numarCurent.getNumarator()));
    ui->line2->setText(QString::number(_numarCurent.getNumitor()));
}

void MainWindow::displayStoredNumber(){
    ui->line1->setText(QString::number(_numarStocat.getNumarator()));
    ui->line2->setText(QString::number(_numarStocat.getNumitor()));
}

/**
 * key input function
 */
void MainWindow::keyReleaseEvent(QKeyEvent *event){

    QLineEdit *field;
    if(ui->line1->isEnabled()){
        field = ui->line1;
    } else {
        field = ui->line2;
    }

    switch (event->key()) {
    case '0':
        if((field->text().length() != 0)&&(field!=ui->line2)){
            field->setText(field->text().append('0'));
        }
        break;
    case '1':
            field->setText(field->text().append('1'));
        break;
    case '2':
            field->setText(field->text().append('2'));
        break;
    case '3':
            field->setText(field->text().append('3'));
        break;
    case '4':
            field->setText(field->text().append('4'));
        break;
    case '5':
            field->setText(field->text().append('5'));
        break;
    case '6':
            field->setText(field->text().append('6'));
        break;
    case '7':
            field->setText(field->text().append('7'));
        break;
    case '8':
            field->setText(field->text().append('8'));
        break;
    case '9':
            field->setText(field->text().append('9'));
        break;
    case Qt::Key_Backspace:
            field->backspace();
        break;
    default:
        break;
    }
}


/**
 * Change field to type function
 */
void MainWindow::on_buttonNrNum_clicked()
{
    if(ui->line1->isEnabled()){
           ui->line1->setEnabled(false);
           ui->line2->setEnabled(true);
       } else {
           ui->line1->setEnabled(true);
           ui->line2->setEnabled(false);
       }
}

/**
 * Delete everything
 */
void MainWindow::on_button_C_clicked()
{
    operatie = NONE;
    ui->line1->setText("");
    ui->line2->setText("");
    _numarCurent.clear();
    _numarStocat.clear();
}

/**
 * Reset number to enter
 */
void MainWindow::on_button_CE_clicked()
{
    ui->line1->setText("");
    ui->line2->setText("");
}

/**
 * @brief MainWindow::on_button_plus_clicked
 *
 * Adds the two first rational numbers
 */
void MainWindow::on_button_plus_clicked()
{
    this->setCurrentNumber();

    ui->line1->setText("");
    ui->line2->setText("");

    this->execLastAction();

    operatie = PLUS;
}

/**
 * @brief MainWindow::on_button_equals_clicked
 *
 * Executes the last operation with the current number
 * and returns into the lineEdits the result
 */
void MainWindow::on_button_equals_clicked()
{
    this->setCurrentNumber();
    this->execLastAction();
    operatie = NONE;
}

/**
 * @brief MainWindow::on_button_minus_clicked
 * Difference btween the 2 numbers
 */
void MainWindow::on_button_minus_clicked()
{
    this->setCurrentNumber();

    ui->line1->setText("");
    ui->line2->setText("");

    this->execLastAction();

    operatie = MINUS;
}

void MainWindow::on_button_multiply_clicked()
{
    this->setCurrentNumber();

    ui->line1->setText("");
    ui->line2->setText("");

    this->execLastAction();

    operatie = MULTI;
}

void MainWindow::on_button_inverse_clicked()
{
    this->setCurrentNumber();
    if(_numarCurent.getNumarator()==0){
        QMessageBox msgBox;
        msgBox.setText("Nu se poate!");
        msgBox.exec();
    } else {
        _numarCurent.inverse();
        this->displayCurrentNumber();
    }
}

void MainWindow::on_button_square_clicked()
{
    this->setCurrentNumber();
    _numarCurent = _numarCurent * _numarCurent;
    this->displayCurrentNumber();
}

void MainWindow::on_button_negate_clicked()
{
    this->setCurrentNumber();
    _numarCurent = -_numarCurent;
    this->displayCurrentNumber();
}

void MainWindow::on_button_divide_clicked()
{
    this->setCurrentNumber();

    ui->line1->setText("");
    ui->line2->setText("");

    this->execLastAction();

    operatie = DIVIDE;
}

void MainWindow::on_memory_plus_clicked()
{
    this->setCurrentNumber();
    _memory = _memory + _numarCurent;
}

void MainWindow::on_memory_minus_clicked()
{
    this->setCurrentNumber();
    _memory = _memory - _numarCurent;
}



void MainWindow::on_memory_clear_clicked()
{
    _memory.clear();
}

void MainWindow::on_memory_recall_clicked()
{
    ui->line1->setText(QString::number(_memory.getNumarator()));
    ui->line2->setText(QString::number(_memory.getNumitor()));
}
