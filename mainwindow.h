#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "rational.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void keyReleaseEvent(QKeyEvent *);

    void on_buttonNrNum_clicked();

    void on_button_C_clicked();

    void numberPressed(QString number);

    void execLastAction();

    void setCurrentNumber();

    void displayCurrentNumber();
    void displayStoredNumber();

    void on_button_plus_clicked();

    void on_button_CE_clicked();

    void on_button_equals_clicked();

    void on_button_minus_clicked();

    void on_button_multiply_clicked();

    void digitPressed();

    void on_button_inverse_clicked();

    void on_button_square_clicked();

    void on_button_negate_clicked();

    void on_button_divide_clicked();

    void on_memory_plus_clicked();

    void on_memory_minus_clicked();

    void on_memory_clear_clicked();

    void on_memory_recall_clicked();

private:
enum Operatii{PLUS,MINUS,EQUAl,DIVIDE,MULTI,NONE};
    Operatii operatie = NONE;
    Ui::MainWindow *ui;
    Rational _numarStocat;
    Rational _numarCurent;
    Rational _memory;
};

#endif // MAINWINDOW_H
