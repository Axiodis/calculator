#ifndef RATIONAL_H
#define RATIONAL_H

#endif // RATIONAL_H

class Rational{

private:
        long long _numarator;
        long long _numitor;

public:
        Rational(){
            _numarator = 0;
            _numitor = 1;
        }

        Rational(long long numarator, long long numitor){
            _numarator = numarator;
            _numitor = numitor;
        }

        long long getNumarator(){
            return _numarator;
        }

        long long getNumitor(){
            return _numitor;
        }

        void setNumber(long long numarator, long long numitor){
            _numarator = numarator;
            _numitor = numitor;
        }

        void clear(){
            _numarator = 0;
            _numitor = 1;
        }

        bool isSet(){
            if((_numarator == 0) && (_numitor == 1)){
                return false;
            } else {
                return true;
            }
        }

        long long cmmdc(){
            long long A = _numarator;
            long long B = _numitor;

            long long Temp;

            while(B)
            {
                Temp = B;
                B = A%B;
                A = Temp;
            }

            return A;
        }

        void simplify(){
            long long cmmdcNumber = this->cmmdc();

            if(cmmdcNumber != 0)
            {
                _numarator = _numarator/cmmdcNumber;
                _numitor = _numitor/cmmdcNumber;
            }
        }

        void inverse(){
            long long aux = _numarator;
            _numarator = _numitor;
            _numitor = aux;
        }

        Rational operator+(Rational x){
            long long numarator;
            long long numitor;

            numitor = this->_numitor * x._numitor;
            numarator = this->_numarator * x._numitor + this->_numitor * x._numarator;

            Rational y(numarator, numitor);
            y.simplify();
            return y;
        }

        Rational operator-(){
            _numarator = -_numarator;
            return *this;
        }

        Rational operator-(Rational x){
            return *this+(-x);
        }

        Rational operator*(Rational x){
            this->_numarator = this->_numarator * x._numarator;
            this->_numitor = this->_numitor * x._numitor;
            this->simplify();
            return *this;
        }

        Rational operator/(Rational x){
            x.inverse();
            *this = *this * x;
            return *this;
        }
};
